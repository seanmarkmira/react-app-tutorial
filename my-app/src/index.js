import React from 'react';
import ReactDom from 'react-dom';
import './index.css';

//Utilization of props
/*
  Props can be utilized to change the values within your react code. This is helpful when you want to utilize different values, displays, texts, and so on to your page.
*/

const firstBook = {
  img:"https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" ,
  title: "This is the title const title variable",
  author: "this is the author value in const author var"
}

const secondBook = {
  img:"https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" ,
  title: "Second Book Title",
  author: "Author Second Book"
}

function BookList(){  
  return(
    <article className ="booklist"> 
      <Book img={firstBook.img} title={firstBook.title} author={firstBook.author}/>
      <Book title={secondBook.title} img={secondBook.img} author={secondBook.author}/>
    </article>
    )
}

//We can also use just props in the book arguement if the properties are just small. but if it is many you can just enter (props) as arguement and access it as props.propertyName or destructure it as show in line 33
const Book = ({img, title, author}) =>{
  // const {img, title, author} = props
  return(<article>
    <img className="imagebook" src={img} alt="this is a book list"/>
      <p>{title}</p>
      <p>{author}</p>
  </article>)
}

ReactDom.render(<BookList/>, document.getElementById('root'))

