import React from 'react';
import ReactDom from 'react-dom';

// Nested Components, React Tools

/*
  Nested Components
    The beauty of React is that you can treat each of the portion of your UI as components and just call the function. See Greeting function, you can see that we called the Person and Message and closing it, from here we can just call the component, change it in their respective functions, and apply the changes without affecting others.
*/


function BookList(){
  return(
    <section>    
      <Image/>
      <Book/>
      <Title/>
      <Author/>
    </section>
    )
}

const Book = () =>{
  return(
    <article>
      This is a book.
    </article>
    )
}

const Title = () => {
  return(
      <h1>Where the crawdads sing</h1>
    )
}
const Image = () => <img src= "https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" alt="this is a book list"/>

const Author = ()=>{
  return(
    <h4>Amelia Hepworth</h4>
    )
}
ReactDom.render(<BookList/>, document.getElementById('root'))


// const Greeting = () =>{
// return(
//   <div>
//     <Person/>
//     <Message/>
//   </div>
//   )
// }
// 
// const Person = () => <h1>john doe</h1>
// const Message = () => {
//   return(
//     <p>This is my message</p>
//     )
// }
// 
// ReactDom.render(<Greeting/>, document.getElementById('root'))