import React from 'react';
import ReactDom from 'react-dom';
import './index.css';

// JS in JSX
// In this example we will be utilizing JavaScript in JSX. The beauty here is that we are able to make use of JS to dynamically show data. For example, author and title is declared here and we can call the value of that author and title to our JSX

const author = "this is the author value in const author var"

function BookList(){
  const title = "This is the title const title variable"
  return(
    <article className ="booklist"> 
      <img className="imagebook" src= "https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" alt="this is a book list"/>
      <h1>Where the crawdads sing</h1>
      <h1>{title}</h1>
      <h4>{author.toUpperCase()}</h4>
      <h4>Amelia Hepworth</h4>
    {/* The code below will not work  since you cannot declare a variable within JSX. The rule must be: anything within {} will always need to return a value. It must be an expression not a statement.*/}
      {/* <p>{let x = 6}</p> */}
      <p>{6+6}</p>
    </article>
    )
}


ReactDom.render(<BookList/>, document.getElementById('root'))

