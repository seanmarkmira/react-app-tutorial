//To create a component
import React from 'react'
//To render the component outside
import ReactDom from 'react-dom'

//This function is a component, for React to know this is a component, first letter must be capitalized
//Remember that in every component, there must be a return value of any HTML even if it is blank

// Reference 1
// const Greeting = () => {
//   // return <h4>This is john and this is my first component</h4>;
//   return React.createElement('h1',{},'hello world')
// }

// Reference 2
// function Greeting(){
//   return(
//     <div>
//       <h1>hello world</h1>
//     </div>
//     )
// }

// Reference 3
const Greeting = () => {
  return React.createElement(
    'div',
    {},
    React.createElement('h1', {}, 'hello world')
    )
}

//passing with arguements: 1. what are we going to pass, 2. where are we going to pass it, in this case the root
ReactDom.render(<Greeting/>, document.getElementById('root'))