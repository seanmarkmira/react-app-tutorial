import React from 'react';
import ReactDom from 'react-dom';
import './index.css';

// inline styles
// See author inline style

function BookList(){
  return(
    <section className ="booklist"> 
      <Image/>
      <Book/>
      <Title/>
      <Author/>
      <Image/>
      <Book/>
      <Title/>
      <Author/>
      <Image/>
      <Book/>
      <Title/>
      <Author/>
    </section>
    )
}

const Book = () =>{
  return(
    <article className="book">
      This is a book.
    </article>
    )
}

const Title = () => {
  return(
      <h1>Where the crawdads sing</h1>
    )
}
const Image = () => <img className="imagebook" src= "https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" alt="this is a book list"/>

/*
  For us to utilize inline style for our html we need to use an object for this and be within a {}. Being within {} means that you are now entering javascript world.

  Take note: the rule of JSX is appled in marginTop and fontSize
*/
const Author = ()=>{
  return(
    <h4 style={{color:'#617d98',fontSize:'0.75rem',marginTop:'0.25rem'}}>Amelia Hepworth</h4>
    )
}
ReactDom.render(<BookList/>, document.getElementById('root'))

