import React from 'react';
import ReactDom from 'react-dom';

/*
JSX Rules
1. return sigle element
2. div / section / article or fragment
3. use camelCase for html atribute
4. className instead of class
5. close every element
6. formatting
*/

//rule 1 and 2
/*
  What this means is that you cannot have 1 div block and use another div block in the return since this will throw 2 elements. For example:

  <div></div>
    <div>
      <ul>
        <li></li>
      </ul>
    </div>

  This does not work and will throw an error. It must be like this:

  <div>
     <div>
      <ul>
        <li></li>
      </ul>
    </div>
  </div>

  But a better approach and readability is to follow rule 2 which states use div - section - article or fragment

  <section>
    <div>
    </div>
  </section>
*/

//rule 3
/*
  onclick should be onClick or else it will not work since we are working with JSX
*/

//rule 4
/*
  class in the html should be written as className = 'something'.

  <div  className='something'>
  
  </div>
*/

//rule 5
/*
  All must be closed. In html/html 5 we can proceed not closing some tags for example: img tags. In React, all must be closed. For example:

  <img src="" alt=""/> rather than just <img src="" alt="">
*/

//rule 6
/*
  Utilize the parenthesis in return. This makes things easier to see what are we returning multiple lines.
*/
const Greeting = () =>{
return(
  <div>
    <h3>hello world</h3>
    <ul>
      <li>
        <a href="#">hello world</a>
      </li>
    </ul>
  </div>
  )
}

ReactDom.render(<Greeting/>, document.getElementById('root'))