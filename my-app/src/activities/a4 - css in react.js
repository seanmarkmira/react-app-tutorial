import React from 'react';
import ReactDom from 'react-dom';


// CSS - to connect, we will have to just import the file
import './a4 - css file.css';

/*
  Rule 3 in JSX, class should be className, class will not work and will throw an error
*/
function BookList(){
  return(
    <section className ="booklist"> 
      <Image/>
      <Book/>
      <Title/>
      <Author/>
      <Image/>
      <Book/>
      <Title/>
      <Author/>
      <Image/>
      <Book/>
      <Title/>
      <Author/>
    </section>
    )
}

const Book = () =>{
  return(
    <article className="book">
      This is a book.
    </article>
    )
}

const Title = () => {
  return(
      <h1>Where the crawdads sing</h1>
    )
}
const Image = () => <img className="imagebook" src= "https://storage.googleapis.com/du-prd/books/images/9780735219090.jpg" alt="this is a book list"/>

const Author = ()=>{
  return(
    <h4>Amelia Hepworth</h4>
    )
}
ReactDom.render(<BookList/>, document.getElementById('root'))

